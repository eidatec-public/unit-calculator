<?php

namespace Tests\Lib\Calculations\Calculators\Weight;

use App\Lib\Calculations\Calculator;
use App\Lib\Calculations\Calculators\IUnitCalculator;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CalculatorTest extends KernelTestCase
{
    public function testLoadCorrectCalculator(): void
    {
        $calculator = new Calculator();

        $this->assertTrue($calculator->getCalculator(Calculator::WEIGHT) instanceof IUnitCalculator);
        $this->assertTrue($calculator->getCalculator(Calculator::LENGTH) instanceof IUnitCalculator);
    }
}
