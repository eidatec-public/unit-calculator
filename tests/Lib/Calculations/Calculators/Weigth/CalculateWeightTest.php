<?php

namespace Tests\Lib\Calculations\Calculators\Weight;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Lib\Calculations\Calculators\Weight\WeightCalculator;

class WeightCalculatorTest extends KernelTestCase
{
    /**
     * @return array
     */
    public static function getTestData(): array
    {
        return [
            [ 0, 0, 'g', 'kg' ],
            [ 23, 23000, 'g', 'mg' ],
            [ 2.56, 2.56, 'kg', 'kg' ],
            [ 4.678, 4678000, 'kg', 'mg' ],
            [ 56, 0.056, 'kg', 't' ],
        ];
    }

    /**
     * @dataProvider getTestData
     */
    public function testWeightCalculation($srcValue, $expectedValue, $srcUnit, $targetUnit): void
    {
        $calculator = new WeightCalculator();

        $this->assertEquals($expectedValue, $calculator->calculate($srcValue, $srcUnit, $targetUnit));
        $this->assertEquals($srcValue, $calculator->calculate($expectedValue, $targetUnit, $srcUnit));
    }
}
