<?php

namespace Tests\Lib\Calculations\Calculators\Length;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Lib\Calculations\Calculators\Length\LengthCalculator;

class LengthCalculatorTest extends KernelTestCase
{
    /**
     * @return array
     */
    public static function getTestData(): array
    {
        return [
            [ 0, 0, 'm', 'km' ],
            [ 23, 23000, 'km', 'm' ],
            [ 2.56, 2.56, 'km', 'km' ],
            [ 4.678, 4678, 'm', 'mm' ],
            [ 56, 0.56, 'cm', 'm' ],
        ];
    }

    /**
     * @dataProvider getTestData
     */
    public function testLengthCalculation($srcValue, $expectedValue, $srcUnit, $targetUnit): void
    {
        $calculator = new LengthCalculator();

        $this->assertEquals($expectedValue, $calculator->calculate($srcValue, $srcUnit, $targetUnit));
        $this->assertEquals($srcValue, $calculator->calculate($expectedValue, $targetUnit, $srcUnit));
    }
}
