<?php

declare(strict_types=1);

namespace App\Lib\Calculations\Calculators\Weight;

use App\Lib\Calculations\Calculators\AbstractUnitCalculator;
use App\Lib\Calculations\Calculators\CalculationParam;

class WeightCalculator extends AbstractUnitCalculator
{
    public function __construct()
    {
        $this->baseUnit = 'g';

        $this->setCalculationParameter('mg', new CalculationParam(CalculationParam::MUL, 1000));
        $this->setCalculationParameter('g', new CalculationParam(CalculationParam::MUL, 1));
        $this->setCalculationParameter('kg', new CalculationParam(CalculationParam::DIV, 1000));
        $this->setCalculationParameter('t', new CalculationParam(CalculationParam::DIV, 1000000));
    }
}
