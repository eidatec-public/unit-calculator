<?php

namespace App\Lib\Calculations\Calculators;

class CalculationParam
{
    public const MUL = '*';
    public const DIV = '/';

    public function __construct(
        public string $operator,
        public float $factor,
    ) {
        if (!\in_array($operator, [self::MUL, self::DIV])) {
            throw new \Exception("Unsupported operator '{$operator}'");
        }
    }

    public function getReverseOperator(): string
    {
        return $this->operator === self::DIV ? self::MUL : self::DIV;
    }
}
