<?php

declare(strict_types=1);

namespace App\Lib\Calculations\Calculators\Length;

use App\Lib\Calculations\Calculators\AbstractUnitCalculator;
use App\Lib\Calculations\Calculators\CalculationParam;

class LengthCalculator extends AbstractUnitCalculator
{
    public function __construct()
    {
        $this->baseUnit = 'm';

        $this->setCalculationParameter('mm', new CalculationParam(CalculationParam::MUL, 1000));
        $this->setCalculationParameter('cm', new CalculationParam(CalculationParam::MUL, 100));
        $this->setCalculationParameter('dm', new CalculationParam(CalculationParam::MUL, 10));
        $this->setCalculationParameter('m', new CalculationParam(CalculationParam::MUL, 1));
        $this->setCalculationParameter('km', new CalculationParam(CalculationParam::DIV, 1000));
    }
}
