<?php

declare(strict_types=1);

namespace App\Lib\Calculations\Calculators;

abstract class AbstractUnitCalculator implements IUnitCalculator
{
    protected array $params = [];
    protected string $baseUnit = '';

    /**
     * Returns the base unit
     *
     * @return string
     */
    public function getBaseUnit(): string
    {
        return $this->baseUnit;
    }

    /**
     * Define Calculation paramenters
     *
     * @param string $unit
     * @param CalculationParam $param
     * @return void
     */
    protected function setCalculationParameter(string $unit, CalculationParam $param): void
    {
        $this->param[$unit] = $param;
    }

    public function calculate(float $srcValue, string $srcUnit, string $targetUnit): float
    {
        if ($srcUnit === $targetUnit) {
            return $srcValue;
        }

        $param = $this->getCalculationParameter($targetUnit);

        return $this->executeCalculation(
            $param->operator,
            $this->toBaseUnit($srcValue, $srcUnit),
            $param->factor
        );
    }

    protected function toBaseUnit(float $srcValue, string $srcUnit): float
    {
        if ($srcUnit === $this->baseUnit) {
            return $srcValue;
        }

        $param = $this->getCalculationParameter($srcUnit);

        return $this->executeCalculation($param->getReverseOperator(), $srcValue, $param->factor);
    }

    protected function executeCalculation(string $operator, float $a, float $factor): float
    {
        return match ($operator) {
            '/' => $a / $factor,
            '*' => $a * $factor,
            default => throw new \Exception("Unsupported operator '{$operator}'"),
        };
    }

    /**
     * Retrieve specific recalculation parameters for this unit
     *
     * @param string $unit
     * @return CalculationParam
     */
    protected function getCalculationParameter(string $unit): CalculationParam
    {
        return $this->param[$unit] ?? throw new \Exception("Unsupported unit '{$unit}'");
    }
}
