<?php

namespace App\Lib\Calculations\Calculators;

interface IUnitCalculator
{
    public function getBaseUnit(): string;
    public function calculate(float $srcValue, string $srcUnit, string $targetUnit): float;
}
