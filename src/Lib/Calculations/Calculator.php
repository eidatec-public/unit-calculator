<?php

namespace App\Lib\Calculations;

use App\Lib\Calculations\Calculators\IUnitCalculator;
use App\Lib\Calculations\Calculators\Length\LengthCalculator;
use App\Lib\Calculations\Calculators\Weight\WeightCalculator;
use App\Lib\Calculations\Collections\CalculatorCollection;

class Calculator
{
    public const WEIGHT = 'weight';
    public const LENGTH = 'length';

    protected CalculatorCollection $collection;

    public function __construct()
    {
        $this->collection = new CalculatorCollection();

        $this->collection->set(self::WEIGHT, new WeightCalculator());
        $this->collection->set(self::LENGTH, new LengthCalculator());
    }

    public function getCalculator(string $key): IUnitCalculator
    {
        return $this->collection->get($key);
    }
}
