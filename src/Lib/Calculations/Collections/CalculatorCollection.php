<?php

declare(strict_types=1);

namespace App\Lib\Calculations\Collections;

use App\Lib\Calculations\Calculators\IUnitCalculator;

class CalculatorCollection
{
    private array $calculators = [];

    public function set(string $key, IUnitCalculator $calculator): void
    {
        $this->calculators[$key] = $calculator;
    }

    public function get(string $key): IUnitCalculator
    {
        return $this->calculators[$key] ?? throw new \Exception("Calculator '{$key}' not found");
    }
}
