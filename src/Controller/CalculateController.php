<?php

namespace App\Controller;

use App\Lib\Calculations\Calculator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CalculateController extends AbstractController
{
    #[Route('/calculate/{type}/{value}/{source}/{target}', name: 'app_calculate', methods: 'GET')]
    public function index(string $type, float $value, string $source, string $target): Response
    {
        $calculator = new Calculator();
        $specificCalculator = $calculator->getCalculator($type);

        $result = $specificCalculator->calculate($value, $source, $target);

        return $this->json([
            'result' => $result,
        ]);
    }
}
