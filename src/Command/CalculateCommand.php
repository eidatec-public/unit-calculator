<?php

namespace App\Command;

use App\Lib\Calculations\Calculator;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'calculate',
    description: 'Recalculate values',
)]
class CalculateCommand extends Command
{
    protected function configure(): void
    {
        $this
            ->addArgument('type', InputArgument::REQUIRED, 'Calculation type to use')
            ->addArgument('value', InputArgument::REQUIRED, 'Value to recalculate')
            ->addArgument('source', InputArgument::REQUIRED, 'Unit of the source value')
            ->addArgument('target', InputArgument::REQUIRED, 'Target unit for the result')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $type = $input->getArgument('type');
        $value = $input->getArgument('value');
        $source = $input->getArgument('source');
        $target = $input->getArgument('target');

        $calculator = new Calculator();
        $specificCalculator = $calculator->getCalculator($type);

        $result = $specificCalculator->calculate($value, $source, $target);

        $output->writeln($result);

        return Command::SUCCESS;
    }
}
