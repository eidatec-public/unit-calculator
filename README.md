# Unit Calculator
Recalculate weight and length units

## Development
Install `devcontainer`
> https://code.visualstudio.com/docs/remote/containers

Begin development
```bash
$ devcontainer open .
```

## Rest

Call calculator in browser
`GET <calculator-endpoint>/calculate/<type>/<value>/source-unit/target-unit`

### Examples:
`http://127.0.0.1:8000/calculate/length/253.56/mm/m`
``` json
{ "result": 0.25356 }
```
`http://127.0.0.1:8000/calculate/weight/5/kg/g`
``` json
{ "result": 5000 }
```

## Commandline
``` bash
$ php bin/console calculate weight 250 kg t

0.25
```